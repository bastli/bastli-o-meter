import React from 'react';
import logo from './bastli-banner.png'
import OpeningHours from './OpeningHours'
import './App.css'

export type State = {
  status: string | null,
  timerID: NodeJS.Timeout | null,
  showLoadingText: boolean
}


export default class App extends React.Component<{}, State> {

  CLOSED = 'closed'
  OPEN = 'open'
  OPEN_UNSUPERVISED = 'open_unsupervised'
  UNKNOWN = 'unknown'

  constructor(props: any) {
    super(props)
    this.state = { status: null, timerID: null, showLoadingText: false }

    setTimeout(() => this._showLoadingText(), 1000)
  }

  componentDidMount() : void {
    this.updateStatus();
    this.setState({timerID: setInterval(
      () => this.updateStatus(),
      5000
    )})
  }

  updateStatus() : void {
    fetch('/api/status/now')
      .then(response => response.json())
      .then(data => this.setState({ status: data.status, showLoadingText: false }))
      .catch(e => this.setState({ status: this.UNKNOWN, showLoadingText: false }))
  }

  _showLoadingText() : void {
    this.setState({ showLoadingText: true })
  }

  _renderWorkshopStatus() : React.ReactNode {
    const state = this.state.status
    const showLoadingText = this.state.showLoadingText

    if (state === this.CLOSED) {
      return (
        <p className="status-container">
          Workshop is <span className="status closed">closed</span>
        </p>
      )
    } else if (state === this.OPEN) {
      return (
        <p className="status-container">
          Workshop is <span className="status open">open</span>
        </p>
      )
    } else if (state === this.OPEN_UNSUPERVISED) {
      return (
        <p className="status-container">
          Workshop is <span className="status open">open</span> but <span className="status unsupervised">unsupervised</span>
        </p>
      )
    } else if (state === null) {
      if (showLoadingText) {
        return (
          <p className="status-container">
            loading <span className="status">...</span>
          </p>
        )
      } else {
        return (
          <p className="status-container">
            <span className="status">&nbsp;</span>
          </p>
        )
      }
    }

    return (
      <p className="status-container">
        Workshop might be <span className="status unknown">closed</span>
      </p>
    )
  }

  render() : React.ReactNode {
    return (
      <div className="app">
        <header className="header">
          <img src={logo} className="logo" alt="Bastli Logo" />
          { this._renderWorkshopStatus() }

          <OpeningHours/>

          <p>More information on <a
            className="link"
            href="https://bastli.ch"
            target="_blank"
            rel="noopener noreferrer"
          >
            bastli.ch
          </a>
          </p>
        </header>
      </div>
    )
  }
}
