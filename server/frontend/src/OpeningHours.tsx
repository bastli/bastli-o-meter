import React from 'react';
import './App.css'

export type State = {
  openingHours: any | null,
  timerID: NodeJS.Timeout | null,
  showLoadingText: boolean
}


export default class OpeningHours extends React.Component<{}, State> {

  DAYS = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday']

  constructor(props: any) {
    super(props)
    this.state = { openingHours: null, timerID: null, showLoadingText: false }

    setTimeout(() => this._showLoadingText(), 1000)
  }

  componentDidMount() : void {
    this.updateOpeningHours();
    this.setState({timerID: setInterval(
      () => this.updateOpeningHours(),
      3600000
    )})
  }

  componentWillUnmount() : void {
    if (this.state.timerID) {
      clearInterval(this.state.timerID)
    }
  }

  updateOpeningHours() : void {
    fetch('/api/openinghours/now')
      .then(response => response.json())
      .then(data => this.setState({ openingHours: data.openinghours, showLoadingText: false }))
      .catch(e => this.setState({ openingHours: null, showLoadingText: false }))
  }

  _showLoadingText() : void {
    this.setState({ showLoadingText: true })
  }

  render() : React.ReactNode {
    const openingHours = this.state.openingHours
    const showLoadingText = this.state.showLoadingText

    if (!openingHours) {
      if (showLoadingText) {
        return (
          <div className="openinghours-container">
            <h2>Opening Hours</h2>
            <span className="openinghours-no-data">loading ...</span>
          </div>
        )
      }
      return (
        <div className="openinghours-container">
          <h2>Opening Hours</h2>
          <span className="openinghours-no-data">&nbsp;</span>
        </div>
      )
    }

    const items = []

    for (let i = 0; i < 7; ++i) {
      const index = String(i)
      if (index in openingHours && openingHours[index] && openingHours[index].length > 0) {
        items.push(
          <div className="openinghours-row">
            <div className="openinghours-weekday">{this.DAYS[i]}</div>
            <div className="openinghours-times">
              {openingHours[index].map((timeslot: any, index: number) => {
                console.log(index)
                let note = ''
                let prefix = index > 0 ? ', ' : ''
                if (timeslot['mode'] === 'open_unsupervised') {
                  note = ` (unsupervised)`
                }
              return <span>{prefix}{timeslot['from']} - {timeslot['to']}{note}</span>
              })}
            </div>
          </div>
        )
      }
    }

    return (
      <div className="openinghours-container">
        <h2>Opening Hours</h2>
        {items}
      </div>
    )
  }
}
