# Flask Backend [REST API] for Bastli Opening Hours Tool

Backend providing the API for the Bastli-o-meter.

## API Documentation

See [documentation of the API endpoints](server/backend/README.API.md).

## Deployment

is done via GitLab CI pipeline. The procedure has to be worked out.

It is very important to mount the file `/etc/localtime` into the docker container to ensure that the timezone is configured correctly.

## Development

Most steps are automated with the script `run.sh`. If you need further instructions for troubleshooting, take a look on the next section.

Please note that all python dependencies are check/updated during environment setup before running the choosen task.

### Creating a local DB for development

You can spin up a local database server very easily with docker.

Use the following command (make sure to replace `%USERNAME%`, `%PASSWORD%` and `%DB_NAME%` in advance!):

```bash
sudo docker run -d --name bastli-o-meter-mariadb \
     -e MYSQL_DATABASE="%DB_NAME%" \
     -e MYSQL_USER="%USERNAME%" \
     -e MYSQL_PASSWORD="%PASSWORD%" \
     -e MYSQL_RANDOM_ROOT_PASSWORD="yes" \
     -v /etc/localtime:/etc/localtime:ro \
     -p 3306:3306 \
     mariadb:10.4
```

In the next step, edit your `config.py`:

* Set `SECRET_KEY` to some random string.
* Set `SQLALCHEMY_DATABASE_URI` to `mysql://%USER%:%PASSWORD%@localhost/%DB_NAME%`.

Last but not least, upgrade the database to the correct state with

```bash
./run.sh db_upgrade
```

This command should be used whenever the database schema has changed and needs to be updated!

### Start Development Server

```bash
./run.sh server
```

### Upgrade Dependencies

```bash
./run.sh dep_upgrade
```

### Create Database Migration Files

```bash
./run.sh db_migrate
```

## Development (Manual steps)

To start the app locally for development, do the following:

1. clone this repo
2. create a python3 virtual environment: `virtualenv venv`
3. and activate it: `source venv/bin/activate`
4. install the requirements inside the virtualenv: `pip install -r requirements.txt`
5. set the following environment variables: `export FLASK_APP="run.py"`, `export FLASK_CONFIG="development"`, and `export FLASK_DEBUG=1`
6. create the local settings file with all the juicy secrets inside in `instance/config.py`. The two following options must be set: `SQLALCHEMY_DATABASE_URI` and `SECRET_KEY`. See next section.
7. run the flask app: `flask run`

After the first initialization of the development environment, you can just run the file `run.sh` to enter that environment and start the development server.

### Creating a local DB for development

You can spin up a local database server very easily with docker.

Use the following command (make sure to replace `%USERNAME%`, `%PASSWORD%` and `%DB_NAME%` in advance!):

```bash
sudo docker run -d --name bastli-o-meter-mariadb \
     -e MYSQL_DATABASE="%DB_NAME%" \
     -e MYSQL_USER="%USERNAME%" \
     -e MYSQL_PASSWORD="%PASSWORD%" \
     -e MYSQL_RANDOM_ROOT_PASSWORD="yes" \
     -p 3306:3306 \
     mariadb:10.4
```

In the next step, edit your `config.py`:

* Set `SECRET_KEY` to some random string.
* Set `SQLALCHEMY_DATABASE_URI` to `mysql://%USER%:%PASSWORD%@localhost/%DB_NAME%`.

Last but not least, upgrade the database to the correct state with `flask db upgrade`.

### Upgrade dependencies

1. Make sure that you have activated the virtual environment with `source venv/bin/activate`.
2. Install pip-tools with `pip install pip-tools`.
3. Run `pip-compile --output-file requirements.txt requirements.in`

### How to handle database changes

Do the following if your code changes require any changes of the database schema:

1. Make sure that your local database schema is equal to the last committed migration file (found in the directory `migrations/`)
2. Generate a new migration file with `flask db migrate`.
3. Apply your changes to the local development database with `flask db upgrade`.
4. Verify that everything is ok with the database schema.
5. Commit the created migrations file. DO NOT CHANGE any migration file which is already committed!
