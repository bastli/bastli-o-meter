# app/amivapi.py

import json
import requests
from . import app

def get(path, token=None, **kwargs):
    return requests.get(app.config.get('AMIV_API_URL') + path, **kwargs, headers={'Authorization': token, 'Content-Type': 'application/json'})


def post(path, data, token=None, **kwargs):
    return requests.get(app.config.get('AMIV_API_URL') + path, **kwargs, data=data, headers={'Authorization': token, 'Content-Type': 'application/json'})


def delete(path, data, token=None, **kwargs):
    return requests.delete(app.config.get('AMIV_API_URL') + path, **kwargs, data=data, headers={'Authorization': token, 'Content-Type': 'application/json'})


def get_selected_groupmemberships(user, groups=[], token=None):
    query = json.dumps({"user": user['_id'], "group": { "$in": groups } })
    response = get('/groupmemberships?where=' + query, token)
    data = response.json()
    if response.status_code == 200:
        return data['_items']
    return []
