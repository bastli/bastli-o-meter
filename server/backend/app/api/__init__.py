# app/api/__init__.py

from flask import Blueprint
from .utils import *

api_bp = Blueprint('api', __name__)

from . import views_status
from . import views_openinghours
