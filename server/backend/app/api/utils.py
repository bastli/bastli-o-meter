# app/api/utils.py

from flask import make_response, jsonify


def make_created(content=None):
    """Prepare created response."""
    if content is None:
        content = jsonify({"_status": "OK"})
    return make_response(content, 201)


def make_no_content():
    """Prepare no content response."""
    return make_response('', 204)
