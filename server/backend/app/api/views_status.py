# app/api/views_status.py

from flask import make_response, request, abort, jsonify
from flask_login import login_required
from datetime import datetime

from .. import app, db, auth
from ..models import StatusReport
from ..models.enums import StatusEnum

from . import api_bp, make_created


@api_bp.route('/status/now', methods = ['GET'])
def check():
    """Get the current status report for the workshop."""
    try:
        last_record = StatusReport.get_last_record()
        return make_response(jsonify({'status': str(last_record.status)}), 200)
    except Exception as e:
        # Make sure to give a valid response even when
        # the database server is not reachable.
        print('Database error: {}'.format(e))

    return make_response(jsonify({'status': str(StatusEnum.UNKNOWN)}), 200)


@api_bp.route('/status', methods = ['GET'])
@auth.userkey_required
def get_status_reports():
    """Get all status report entries."""
    # Checks timeout of last record update.
    StatusReport.check_last_record()
    response = db.session.query(StatusReport).order_by(StatusReport.created)
    items = []
    for item in response:
        items.append(item)
    return make_response(jsonify({
        '_items': items
    }), 200)

@api_bp.route('/status/<int:id>', methods = ['GET'])
@auth.userkey_required
def get_status_report(id):
    """Get a specific status report entry."""
    item = db.session.query(StatusReport).get(id)
    if item is None:
        abort(404)
    return make_response(jsonify(item), 200)


@api_bp.route('/status', methods = ['POST'])
@auth.key_required
def post_status_report():
    """Create or update the last record of status report entry."""
    data = request.get_json()
    issues = {}
    status = None

    # Validate POST data
    if not data or not data.get('status'):
        issues['status'] = 'value must not be null or empty'
    else:
        label = data.get('status')
        status = StatusEnum.from_str(label)
        if status == None:
            issues['status'] = 'unallowed value {}'.format(label)

    if len(issues) > 0:
        abort(422, issues)

    last_record = StatusReport.get_last_record()

    if last_record.status == status:
        last_record.updated = db.func.now()
        item = last_record
    else:
        item = StatusReport()
        item.status = status

        db.session.add(item)
    db.session.commit()

    return make_created(jsonify(item))
