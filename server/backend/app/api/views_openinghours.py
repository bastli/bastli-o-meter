# app/api/views_openinghours.py

from flask import make_response, request, abort, jsonify
from datetime import datetime, timedelta

from .. import app, db, auth
from . import api_bp, make_created, make_no_content
from ..models import OpeningHours
from ..models.enums import StatusEnum


@api_bp.route('/openinghours/now', methods = ['GET'], defaults={'date': None})
@api_bp.route('/openinghours/<string:date>', methods = ['GET'])
def get_opening_hours_on_date(date):
    """Get the opening hours for the seven days starting at the given date.

    Args:
      - date: Date string formatted as YYYY-MM-DD
    """
    if date is not None:
        try:
            start_date = datetime.strptime(date, "%Y-%m-%d")
        except:
            abort(400, 'Invalid date format. Must be yyyy-mm-dd.')
    else:
        start_date = datetime.now()

    openinghours = {}

    for day in range(7):
        weekday = start_date.weekday()
        if weekday > day:
            validity_time = start_date.date() + timedelta(days=7-weekday+day)
        else:
            validity_time = start_date.date() + timedelta(days=day)
        response = db.session.query(OpeningHours) \
            .filter(OpeningHours.day == day, OpeningHours.validity_time_start <= validity_time, OpeningHours.validity_time_end >= validity_time) \
            .order_by(OpeningHours.validity_time_start)
        slots = []
        exclusive_only = False
        for entry in response.all():
            slot = {
                'from': entry.time_start.strftime('%H:%M'),
                'to': entry.time_end.strftime('%H:%M'),
                'mode': str(entry.status)
            }
            if entry.exclusive:
                exclusive_only = True
                if entry.status != StatusEnum.CLOSED:
                    slots = [slot]
                else:
                    slots = []
            elif not exclusive_only and entry.status != StatusEnum.CLOSED:
                slots.append(slot)

        if len(slots) > 0:
            openinghours[day] = slots

    response = {
        'openinghours': openinghours,
    }

    return make_response(jsonify(response), 200)


@api_bp.route('/openinghours', methods = ['GET'])
@auth.userkey_required
def get_opening_hours():
    """Get the opening hour entries."""
    response = db.session.query(OpeningHours).order_by(OpeningHours.timestamp)
    items = []
    for item in response:
        items.append(item)
    return make_response(jsonify({
        '_items': items
    }), 200)


@api_bp.route('/openinghours/<int:id>', methods = ['GET'])
@auth.userkey_required
def get_opening_hour(id):
    """Get a specific opening hour entry."""
    item = db.session.query(OpeningHours).get(id)
    if item is None:
        abort(404)
    return make_response(jsonify(item), 200)


@api_bp.route('/openinghours', methods = ['POST'])
@auth.userkey_required
def post_opening_hours():
    """Create a new opening hours entry."""
    data = request.get_json()

    # Validate POST data
    item, issues = OpeningHours.from_dict(data)
    if len(issues) > 0:
        abort(422, issues)

    if item is None:
        abort(500)

    db.session.add(item)
    db.session.commit()
    return make_created(jsonify(item))


@api_bp.route('/openinghours/<int:id>', methods = ['PATCH'])
@auth.userkey_required
def patch_opening_hours(id):
    """Get the opening hours."""
    data = request.get_json()

    # Validate POST data
    patched_item, issues = OpeningHours.from_dict(data, ignore_missing=True)

    if patched_item is None:
        abort(500)

    existing_item = db.session.query(OpeningHours).get(id)
    if existing_item is None:
        abort(404)

    # Merge items
    if 'status' in data and 'status' not in issues:
        existing_item.status = patched_item.status

    if 'exclusive' in data and 'exclusive' not in issues:
        existing_item.exclusive = patched_item.exclusive

    if 'day' in data and 'day' not in issues:
        existing_item.day = patched_item.day

    if 'time_start' in data and 'time_start' not in issues:
        existing_item.time_start = patched_item.time_start

    if 'time_end' in data and 'time_end' not in issues:
        existing_item.time_end = patched_item.time_end

    if 'validity_time_start' in data and 'validity_time_start' not in issues:
        existing_item.validity_time_start = patched_item.validity_time_start

    if 'validity_time_end' in data and 'validity_time_end' not in issues:
        existing_item.validity_time_end = patched_item.validity_time_end

    db.session.commit()

    return make_response(jsonify(existing_item))


@api_bp.route('/openinghours/<int:id>', methods = ['DELETE'])
@auth.userkey_required
def delete_opening_hours(id):
    """Delete an opening hours entry."""
    db.session.delete(db.session.query(OpeningHours).get(id))
    db.session.commit()
    return make_no_content()
