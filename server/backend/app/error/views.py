# app/error/views.py

from flask import abort, jsonify

from .. import app


@app.route("/<path:invalid_path>")
def dummy_page(invalid_path):
    abort(404)


@app.errorhandler(400)
def page_bad_request(e):
    if (e.description):
        description = e.description
    else:
        description = 'The server could not understand your request.'

    return jsonify({
        '_status': 'ERR',
        '_error': {
            'code': 400,
            'message': description,
        }
    }), 401


@app.errorhandler(401)
def page_unauthorized(e):
    if (e.description):
        description = e.description
    else:
        description = 'The server could not verify that you are authorized to access the ' \
            'URL requested. You either supplied the wrong credentials (e.g. a bad password), ' \
            'or your browser doesn\'t understand how to supply the credentials required.'

    return jsonify({
        '_status': 'ERR',
        '_error': {
            'code': 401,
            'message': description,
        }
    }), 401


@app.errorhandler(403)
def page_forbidden(e):
    return jsonify({
        '_status': 'ERR',
        '_error': {
            'code': 403,
            'message': e.description,
        }
    }), 403


@app.errorhandler(404)
def page_not_found(e):
    return jsonify({
        '_status': 'ERR',
        '_error': {
            'code': 404,
            'message': e.description,
        }
    }), 404


@app.errorhandler(422)
def page_unprocessable(e):
    return jsonify({
        '_status': 'ERR',
        '_issues': e.description,
        '_error': {
            'code': 422,
            'message': 'Insertion failure: document contains error(s)',
        }
    }), 422


@app.errorhandler(500)
def page_internal_error(e):
    return jsonify({
        '_status': 'ERR',
        '_error': {
            'code': 500,
            'message': e.description,
        }
    }), 500
