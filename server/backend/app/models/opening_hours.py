# app/models/opening_hours.py

from datetime import datetime
import json

from .. import db
from ..models.enums import StatusEnum


class OpeningHours(db.Model):
    """
    Opening hours entry entry.
    """

    __tablename__ = 'opening_hours'

    _id = db.Column(db.Integer, primary_key=True)
    status = db.Column(db.Enum(StatusEnum), nullable=False)
    exclusive = db.Column(db.Boolean, default=True)
    day = db.Column(db.Integer, nullable=False)
    time_start = db.Column(db.Time, nullable=False)
    time_end = db.Column(db.Time, nullable=False)
    validity_time_start = db.Column(db.DateTime, nullable=False)
    validity_time_end = db.Column(db.DateTime, nullable=True)
    timestamp = db.Column(db.DateTime, nullable=False, server_default=db.func.now())

    # Indexes

    __table_args__ = (
        db.Index('opening_hours_validity_idx', validity_time_start, validity_time_end),
        db.Index('opening_hours_timestamp_idx', timestamp),
    )


    def to_dict(self):
        data = {
            '_id': self._id,
            'status': str(self.status),
            'exclusive': self.exclusive,
            'day': self.day,
            'time_start': self.time_start.strftime('%H:%M'),
            'time_end': self.time_end.strftime('%H:%M'),
            'validity_time_start': self.validity_time_start.strftime('%Y-%m-%d'),
            'validity_time_end': self.validity_time_end.strftime('%Y-%m-%d'),
            'timestamp': self.timestamp.isoformat()
        }
        return data


    def _jsonSupport(*args):
        def default(self, xObject):
            data = xObject.to_dict()
            return data
        json.JSONEncoder.default = default

    _jsonSupport()


    def __str__(self):
        return json.dumps(self.to_dict())


    def __repr__(self):
        return '<%s.%s>' % (self.__class__.__name__, self._id)


    @staticmethod
    def from_dict(data: dict, ignore_missing: bool = False):
        item = OpeningHours()
        issues = {}

        item.status = OpeningHours._parse_status_field(data, 'status', issues, ignore_missing)
        item.exclusive = OpeningHours._parse_boolean_field(data, 'exclusive', issues, ignore_missing)
        item.day = OpeningHours._parse_int_field(data, 'day', issues, ignore_missing)
        item.time_start = OpeningHours._parse_time_field(data, 'time_start', issues, ignore_missing)
        item.time_end = OpeningHours._parse_time_field(data, 'time_end', issues, ignore_missing)
        item.validity_time_start = OpeningHours._parse_datetime_field(data, 'validity_time_start', issues, ignore_missing)
        item.validity_time_end = OpeningHours._parse_datetime_field(data, 'validity_time_end', issues, ignore_missing)

        return item, issues


    @staticmethod
    def _check_general_field(data, field_name, issues, ignore_missing):
        if not data or data.get(field_name) is None:
            if not ignore_missing:
                issues[field_name] = 'value must not be null or empty'
            return False
        return True

    @staticmethod
    def _parse_status_field(data, field_name, issues, ignore_missing):
        if not OpeningHours._check_general_field(data, field_name, issues, ignore_missing):
            return None
        status = StatusEnum.from_str(data.get(field_name))
        if status == None:
            issues[field_name] = 'unallowed value {}'.format(label)
        return status

    @staticmethod
    def _parse_boolean_field(data, field_name, issues, ignore_missing):
        if OpeningHours._check_general_field(data, field_name, issues, ignore_missing):
            label = data.get(field_name)
            try:
                return bool(label)
            except:
                issues[field_name] = 'unallowed value {}'.format(label)
        return False

    @staticmethod
    def _parse_int_field(data, field_name, issues, ignore_missing):
        if OpeningHours._check_general_field(data, field_name, issues, ignore_missing):
            label = data.get(field_name)
            try:
                return int(label)
            except:
                issues[field_name] = 'unallowed value {}'.format(label)
        return 0

    @staticmethod
    def _parse_time_field(data, field_name, issues, ignore_missing):
        if OpeningHours._check_general_field(data, field_name, issues, ignore_missing):
            label = data.get(field_name)
            try:
                return datetime.strptime(label, "%H:%M")
            except Exception as e:
                issues[field_name] = 'unallowed value {}. Must be of format HH:MM'.format(label)
        return None
    
    @staticmethod
    def _parse_datetime_field(data, field_name, issues, ignore_missing):
        if OpeningHours._check_general_field(data, field_name, issues, ignore_missing):
            label = data.get(field_name)
            try:
                return datetime.strptime(label, "%Y-%m-%d")
            except:
                issues[field_name] = 'unallowed value {}. Must be of format yyyy-mm-dd.'.format(label)
        return None
