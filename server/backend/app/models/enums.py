# app/models/enums.py

import json

from enum import Enum


class BaseEnum(Enum):
    def __str__(self):
        return str(self.value)
    
    def _jsonSupport( *args ):
        def default( self, xObject ):
            return str(xObject)
        json.JSONEncoder.default = default

    _jsonSupport()

    def __repr__(self):
        return '<%s.%s>' % (self.__class__.__name__, self.name)


class StatusEnum(BaseEnum):
    CLOSED            = 'closed'
    OPEN              = 'open'
    OPEN_UNSUPERVISED = 'open_unsupervised'
    UNKNOWN           = 'unknown'

    @staticmethod
    def from_str(label):
        if label == str(StatusEnum.CLOSED):
            return StatusEnum.CLOSED
        if label == str(StatusEnum.OPEN):
            return StatusEnum.OPEN
        if label == str(StatusEnum.OPEN_UNSUPERVISED):
            return StatusEnum.OPEN_UNSUPERVISED
        if label == str(StatusEnum.UNKNOWN):
            return StatusEnum.UNKNOWN
        return None

    @classmethod
    def choices(cls):
        return [(choice, choice.value) for choice in cls]

    @classmethod
    def coerce(cls, item):
        return item if (item == None or type(item) == StatusEnum) else StatusEnum[item]
