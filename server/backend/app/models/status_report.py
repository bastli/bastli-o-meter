# app/models/status_report.py
import json
from datetime import datetime

from .. import app, db
from ..models.enums import StatusEnum


class StatusReport(db.Model):
    """
    Status change log entry.
    """

    __tablename__ = 'status_log'

    _id = db.Column(db.Integer, primary_key=True)
    status = db.Column(db.Enum(StatusEnum), nullable=False)
    created = db.Column(db.DateTime, nullable=False, server_default=db.func.now())
    updated = db.Column(db.DateTime, nullable=False, server_default=db.func.now(), onupdate=db.func.now())

    # Indexes

    __table_args__ = (
        db.Index('status_log_created_idx', created),
        db.Index('status_log_updated_idx', updated),
    )

    def to_dict(self):
        data = {
            '_id': self._id,
            'status': str(self.status),
            'created': self.created.isoformat(),
            'updated': self.updated.isoformat()
        }
        return data

    def _jsonSupport(*args):
        def default(self, xObject):
            data = xObject.to_dict()
            return data
        json.JSONEncoder.default = default

    _jsonSupport()

    @staticmethod
    def check_last_record():
        """Check last record and add entry with status UNKNOWN is necessary. Does not return anything."""
        StatusReport.get_last_record()

    @staticmethod
    def get_last_record():
        """Get last status log entry.
        If the last entry is more than update interval ago, a new entry with state UNKNOWN is created."""
        last_record = db.session.query(StatusReport).order_by(StatusReport.created.desc()).first()

        interval = app.config['SENSOR_MAX_UPDATE_INTERVAL']

        if last_record is None or \
            (last_record.status != StatusEnum.UNKNOWN and \
                datetime.now() - last_record.updated > interval):
            if last_record is None:
                new_date = datetime.now()
            else:
                new_date = last_record.updated + interval
            report = StatusReport()
            report.status = StatusEnum.UNKNOWN
            report.created = new_date
            report.updated = new_date

            db.session.add(report)
            db.session.commit()
            return report
        return last_record
