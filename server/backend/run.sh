#!/bin/sh

# Virtual environment name (name of subdirectory for venv files)
VIRTUALENV="venv"

RED="\033[0;31m"
GREEN="\033[0;32m"
YELLOW="\033[1;33m"
NC="\033[0m" # No Color

# ----------------------------
# Helper functions
# ----------------------------

# Write error message to stderr
# Parameters:
#    $1: error code
#    $2: error message
#
# Example: printerr $errorcode "An error happened!"
printerr() { printf "[${RED}ERROR${NC} %i] %s\n" "$1" "$2" 1>&2; }

# Write a warning to stdout
# Parameters:
#    $1: message text
#
# Example: printwarn "This is a warning!"
printwarn() { printf "[${YELLOW}WARN${NC}] %s\n" "$1"; }

# Write a warning to stdout
# Parameters:
#    $1: message text
#
# Example: printsuccess "This is a warning!"
printsuccess() { printf "[${GREEN}SUCCESS${NC}] %s\n" "$1"; }

# Write info message to stdout
# Parameters:
#    $1: error message
#
# Example: printinfo "An error happened!"
printinfo() { printf "[INFO] %s\n" "$1"; }


# ----------------------------
# Actual script below
# ----------------------------


# Read selection

task="$1"

TASK_SERVER="server"
TASK_DEP_UPGRADE="dep_upgrade"
TASK_DB_UPGRADE="db_upgrade"
TASK_DB_MIGRATE="db_migrate"

if [ "$task" = "$TASK_SERVER" ] || [ "$task" = "$TASK_DEP_UPGRADE" ] || [ "$task" = "$TASK_DB_UPGRADE" ] || [ "$task" = "$TASK_DB_MIGRATE" ]; then
    printinfo "Task \"$task\" selected."
else
    printerr 1 "Unknown task \"$task\"."
    printf "\nMust be one of \"$TASK_SERVER\", \"$TASK_DEP_INSTALL\", \"$TASK_DEP_UPGRADE\", \"$TASK_DB_UPGRADE\" or \"$TASK_DB_MIGRATE\".\n"
    exit 1
fi

printinfo "Preparing development environment..."

# Prepare environment
export FLASK_APP=run.py
export FLASK_CONFIG="development"
export FLASK_DEBUG=1

if ! test -f "$VIRTUALENV/bin/activate"; then
    printinfo "Virtual environment not found. Creating..."
    virtualenv -p /usr/bin/python3 venv > /dev/null
    retval=$?
    if [ $retval -eq 0 ]; then
        printf "[${GREEN}CREATED${NC}] Development Environment created.\n"
    else
        printerr $retval "Failed to create a new development environment. Abort."
        exit 1 
    fi
fi

source "$VIRTUALENV/bin/activate"

retval=$?
if ! [ $retval -eq 0 ]; then
    printerr $retval "Failed to load development environment. Abort."
    exit 1 
fi

printinfo "Installing python dependencies..."

pip install -r requirements.txt > /dev/null

retval=$?
if [ $retval -eq 0 ]; then
    printsuccess "Python dependencies installed."
else
    printwarn "Failed to install python dependencies. Some dependencies might be missing!"
fi

printf "[${GREEN}READY${NC}] Development Environment is set up.\n\n"

if [ "$task" = "$TASK_SERVER" ]; then
    # Run flask development server
    flask run
    printsuccess "Development Server terminated."
elif [ "$task" = "$TASK_DEP_UPGRADE" ]; then
    pip install pip-tools > /dev/null
    retval=$?
    if [ $retval -eq 0 ]; then
        pip-compile --output-file requirements.txt requirements.in && pip install -r requirements.txt > /dev/null
        retval=$?
        if [ $retval -eq 0 ]; then
            printsuccess "Upgraded and installed dependencies (requirements.txt)."
        else
            printerr $retval "Failed to upgrade dependencies. Abort."
            exit 1
        fi
    else
        printerr $retval "Failed to install required python depndency. Abort."
        exit 1
    fi
elif [ "$task" = "$TASK_DB_UPGRADE" ]; then
    flask db upgrade
    retval=$?
    if [ $retval -eq 0 ]; then
        printsuccess "Local database schema updated."
    else
        printerr $retval "Failed to update local database schema. Abort."
        exit 1
    fi
elif [ "$task" = "$TASK_DB_MIGRATE" ]; then
    flask db migrate
    retval=$?
    if [ $retval -eq 0 ]; then
        printsuccess "New migrations files generated."
    else
        printerr $retval "Failed to create new migrations files. Abort."
        exit 1
    fi
else
    printerr 1 "Invalid selection. Abort."
    exit 1
fi
