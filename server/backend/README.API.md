# API Endpoints - Flask Backend [REST API] for Bastli Opening Hours Tool

## Status

This resource represents the current workshop status (burnt down is not an option ;).

### [GET] /status/now

Returns the current status of the workshop formatted as JSON.

Example:

```json
{
    "status": "open"
}
```

### [GET] /status

Returns a list of all status reports of the workshop formatted as JSON.

Headers: `Authorization` with AMIVAPI user token.

Example:

```json
{
  "_items": [
    {
      "_id": 1,
      "created": "2020-01-04T12:31:16",
      "status": "unknown",
      "updated": "2020-01-04T12:31:16"
    },
    {
      "_id": 2,
      "created": "2020-01-04T12:31:48",
      "status": "open",
      "updated": "2020-01-04T12:31:58"
    },
  ]
}
```

### [GET] /status/\<id\>

Returns a single status report of the workshop formatted as JSON.

Headers: `Authorization` with AMIVAPI user token.

Example:

```json
{
  "_id": 1,
  "created": "2020-01-04T12:31:16",
  "status": "unknown",
  "updated": "2020-01-04T12:31:16"
}
```

### [POST] /status

Creates a new status report or updates the last record if the status has not changed since then.
Data must be formatted as JSON as below.

Headers: `Authorization` with API key or AMIVAPI user token.

Example:

```json
{
  "status": "open"
}
```

## Opening Hours

This resource represents the opening hours of the workshop.

### [GET] /openinghours/now or /openinghours/yyyy-mm-dd

Returns the opening hours of the seven days after the given date formatted as JSON.
The number keys in the object represent the weekdays.

Example:

```json
{
  "openinghours": {
    "1": [
      {
        "from": "17:00",
        "mode": "open_unsupervised",
        "to": "20:00"
      },
      {
        "from": "12:15",
        "mode": "open",
        "to": "13:00"
      }
    ],
    "3": [
      {
        "from": "12:15",
        "mode": "open",
        "to": "13:00"
      }
    ]
  }
}
```

### [GET] /openinghours

Returns a list of all opening hour entries formatted as JSON.

Headers: `Authorization` with an AMIVAPI user token.

Example:

```json
{
  "_items": [
    {
      "_id": 1,
      "day": 1,
      "exclusive": false,
      "status": "open_unsupervised",
      "time_end": "20:00",
      "time_start": "17:00",
      "timestamp": "2020-01-04T12:34:05",
      "validity_time_end": "2020-02-19",
      "validity_time_start": "2020-01-02"
    },
    {
      "_id": 2,
      "day": 0,
      "exclusive": false,
      "status": "open",
      "time_end": "13:00",
      "time_start": "12:15",
      "timestamp": "2020-01-04T12:34:21",
      "validity_time_end": "2020-02-19",
      "validity_time_start": "2020-01-02"
    }
  ]
}
```

### [GET] /openinghours/\<id\>

Returns a single opening hour entry formatted as JSON.

Headers: `Authorization` with an AMIVAPI user token.

Example:

```json
{
  "_id": 1,
  "day": 1,
  "exclusive": false,
  "status": "open_unsupervised",
  "time_end": "20:00",
  "time_start": "17:00",
  "timestamp": "2020-01-04T12:34:05",
  "validity_time_end": "2020-02-19",
  "validity_time_start": "2020-01-02"
}
```

### [POST] /openinghours

Creates a new opening hour entry. Data must be formatted as JSON as below.

Headers: `Authorization` with an AMIVAPI user token.

Example:

```json
{
    "status": "open",
    "exclusive": false,
    "day": 0,
    "time_start": "12:15",
    "time_end": "13:00",
    "validity_time_start": "2020-01-02",
    "validity_time_end": "2020-01-10"
}
```

### [PATCH] /openinghours/\<id\>

Updates an existing opening hour by merging both objects together.
Only data sent with the request is changed!
Data must be formatted as JSON as below.

Headers: `Authorization` with an AMIVAPI user token.

Example:

```json
{
  "validity_time_end": "2020-01-15"
}
```

### [DELETE] /openinghours/\<id\>

Deletes a specific opening hour entry.

Headers: `Authorization` with an AMIVAPI user token.
