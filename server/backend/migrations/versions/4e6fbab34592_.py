"""Add status_log

Revision ID: 4e6fbab34592
Revises: 
Create Date: 2019-12-30 16:05:01.832143

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '4e6fbab34592'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('status_log',
    sa.Column('_id', sa.Integer(), nullable=False),
    sa.Column('status', sa.Enum('CLOSED', 'OPEN', 'UNKNOWN', name='statusenum'), nullable=False),
    sa.Column('timestamp', sa.DateTime(), server_default=sa.text('now()'), nullable=False),
    sa.PrimaryKeyConstraint('_id')
    )
    op.create_index('status_log_timestamp_idx', 'status_log', ['timestamp'], unique=False)
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_index('status_log_timestamp_idx', table_name='status_log')
    op.drop_table('status_log')
    # ### end Alembic commands ###
