# Bastli-o-meter server part

This directory contains the frontend and backend source code to be hosted on a container hosting server.

## Backend

The backend consists of a REST API. See [Backend Documentation](server/backend/README.md).

## Frontend

The frontend is a React.js web application. See [Frontend Documentation](server/frontend/README.md).
