# Bastli-o-meter

The bastli-o-meter provides the information whether the Bastli is currently open.

It consists of two parts. The sensor part running on a Raspberry Pi which regularly reports whether the Bastli is open. The second part is the backend providing the API.

## Server Part

The server applications should be hosted on a container hosting server. See [Server Part Documentation](server/README.md).
